package Model;

import javax.persistence.*;

@Entity
@Table(name="regions")
public class Region {

    @Id
    @Column(name = "region_id")
    private int id;

    @Column(name = "region_name")
    private String regionName;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getRegionName() {
        return regionName;
    }

    public void setRegionName(String regionName) {
        this.regionName = regionName;
    }
}
